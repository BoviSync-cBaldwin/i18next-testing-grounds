window.i18next.init({
  lng: 'en',
  debug: true,
  resources: {
    en: {
      translation: {
        "static text": "This is static text.",
        "Text from scope": "This is text from the scope.",
        "one": "123 Dies ist ein Text aus dem Anwendungsbereich.",
      }
    },
    de: {
      translation: {
        "static text": "Dies ist statischer Text.",
        "Text from scope": "Dies ist ein Text aus dem Anwendungsbereich.",
        "one": "123 Dies ist ein Text aus dem Anwendungsbereich.",
      }
    }
  }
});

angular.module('i18nextTestApp', ['jm.i18next'])
  .controller('mainCtrl', ($scope, $i18next) => {
    $scope.textFromScope = $i18next.t('one');

    $scope.changeLanguage = () => {
      if ( i18next.language === 'en' ){
        window.i18next.changeLanguage('de');
      }
      else {
        window.i18next.changeLanguage('en');
      }
    };
  });
